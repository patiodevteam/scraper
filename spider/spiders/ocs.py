import scrapy
import datetime
import json
from scrapy.mail import MailSender
from scrapy.spiders import SitemapSpider

mailer = MailSender()

class OCSSpider(SitemapSpider):
    name = "ocs"
    sitemap_urls = ['https://ocs.ca/sitemap.xml']

    sitemap_rules = [
        ('/products/', 'parse_product'),
    ]

    sitemap_follow = ['/sitemap_products']

    def parse_product(self, response):
        pattern = r'window\.theme\.product_json\s*=\s*(\{.*?\})\s*;\s*\n'
        product_json = response.css('script::text').re_first(pattern)
        products = json.loads(product_json)

        for product in products['variants']:
            data = {}
            data['title'] = products['title']
            data['name'] = product['name']
            data['gtin'] = product['barcode']
            data['sku'] = product['sku']
            data['id'] = product['id']
            data['plant_type'] = response.css('.product__properties li:nth-child(3) p::text').extract()
            data['terpenes'] = response.css('.terpene__list span::text').extract()
            data['thc'] = response.css('.product__properties li:nth-child(1) p::text').extract()
            data['cbd'] = response.css('.product__properties li:nth-child(2) p::text').extract()
            data['weight'] = product['public_title']
            data['price'] = product['price']
            data['price_min'] = products['price_min']
            data['price_max'] = products['price_max']
            data['description'] = products['description']
            data['vendor'] = products['vendor']
            data['type'] = products['type']
            data['images'] = products['images']
            data['featured_image'] = products['featured_image']
            data['url_ocs'] = response.url
            data['url_leafly'] = ''
            data['created_at'] = products['created_at']
            data['published_at'] = products['published_at']
            data['last_successful_scrape_at'] = datetime.datetime.now()
            yield data

