import scrapy
import datetime
import json
from scrapy.mail import MailSender
from scrapy.spiders import SitemapSpider

mailer = MailSender()

class BCCSSpider(SitemapSpider):
    name = "bccs"
    sitemap_urls = ['https://www.bccannabisstores.com/sitemap.xml']

    sitemap_rules = [
        ('/products/', 'parse_product'),
    ]

    sitemap_follow = ['/sitemap_products']

    def parse_product(self, response):
        pattern = r'"product":\s*(\{.*?\})\n'
        product_json = response.css('#shopify-section-static-product script::text').re_first(pattern)
        products = json.loads(product_json)

        for product in products['variants']:
            data = {}
            data['title'] = products['title']
            data['name'] = product['name']
            data['gtin'] = product['barcode']
            data['sku'] = product['sku']
            data['id'] = product['id']
            data['plant_type'] = response.css('.product-characteristics li:nth-child(5) span.product-characteristics--right::text').extract()
            data['terpenes'] = response.css('.product-terpenes-overview li::text').extract()
            data['thc'] = response.css('.product-characteristics li:nth-child(6) span.product-characteristics--right::text').extract()
            data['cbd'] = response.css('.product-characteristics li:nth-child(7) span.product-characteristics--right::text').extract()
            data['weight'] = product['public_title']
            data['price'] = product['price']
            data['price_min'] = products['price_min']
            data['price_max'] = products['price_max']
            data['description'] = products['description']
            data['vendor'] = products['vendor']
            data['type'] = products['type']
            data['images'] = products['images']
            data['featured_image'] = product['featured_image']
            data['url_bccs'] = response.url
            data['created_at'] = products['created_at']
            data['published_at'] = products['published_at']
            data['last_successful_scrape_at'] = datetime.datetime.now()
            yield data
