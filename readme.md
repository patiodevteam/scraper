# Scrapy for Scannaapp

[Follow installation installation instructions on your platform](https://docs.scrapy.org/en/latest/intro/install.html#installing-scrapy)

## Spiders

List of available spiders (set to crawl) and launch its custom parsers

* ocs (scanns OCS Vendor)
* bccs (scanns BCCannabisStores Vendor)

```
$ scrapy crawl [spider]

e.g.:

$ scrapy crawl ocs
$ scrapy crawl bccs
```

## Export to CSV

```
$ scrapy crawl ocs -o ocs.csv
```

Use `--no-log` tu suppress script verbosity

## Preview scrape

```
$ scrapy shell [url]

e.g.:

$ scrapy shell [https://www.google.com]
```

## Sample products json

```
{
  "id": 1314098218828,
  "title": "CBD Shark Shock",
  "handle": "cbd-shark-shock-redecan",
  "description": "Redecan’s CBD Shark Shock is a pairing of White Widow and Skunk #1. This strain, from Ontario’s Niagara region, has very mild THC potency and compact buds covered in resin. Its aroma is dank, fruity and sweet, and it tastes like green apple, sour citrus, sweet apricot and eucalyptus.\n\n",
  "published_at": "2018-10-16T13:10:35-04:00",
  "created_at": "2018-09-13T15:36:11-04:00",
  "vendor": "Redecan",
  "type": "Cannabis",
  "tags": [
    "category--Cannabis",
    "cbd_content_max--9.000000",
    "cbd_content_min--6.000000",
    "gtin--00628242240710",
    "gtin--00628242240727",
    "gtin--00628242240734",
    "gtin--00628242240741",
    "has_cbd_content",
    "has_thc_content",
    "language--en",
    "plant_type--Hybrid",
    "subcategory--Dried Flower",
    "subsubcategory--Whole Flower",
    "terpenes--Myrcene",
    "terpenes--Ocimene",
    "terpenes--Terpinolene",
    "terpenes--Trans-Caryophyllene",
    "Test-06\\/07\\/2018",
    "thc_content_max--5.000000",
    "thc_content_min--2.000000"
  ],
  "price": 975,
  "price_min": 975,
  "price_max": 13180,
  "available": true,
  "price_varies": true,
  "compare_at_price": 900,
  "compare_at_price_min": 900,
  "compare_at_price_max": 13050,
  "compare_at_price_varies": true,
  "variants": [
    {
      "id": 12175424128844,
      "title": "1g",
      "option1": "1g",
      "option2": null,
      "option3": null,
      "sku": "00628242240710",
      "requires_shipping": true,
      "taxable": true,
      "featured_image": null,
      "available": true,
      "name": "CBD Shark Shock - 1g",
      "public_title": "1g",
      "options": [
        "1g"
      ],
      "price": 975,
      "weight": 0,
      "compare_at_price": 900,
      "inventory_management": "shopify",
      "barcode": "00628242240710"
    },
    {
      "id": 12175424161612,
      "title": "3.5g",
      "option1": "3.5g",
      "option2": null,
      "option3": null,
      "sku": "00628242240727",
      "requires_shipping": true,
      "taxable": true,
      "featured_image": null,
      "available": true,
      "name": "CBD Shark Shock - 3.5g",
      "public_title": "3.5g",
      "options": [
        "3.5g"
      ],
      "price": 3240,
      "weight": 0,
      "compare_at_price": 3325,
      "inventory_management": "shopify",
      "barcode": "00628242240727"
    },
    {
      "id": 12175424194380,
      "title": "7g",
      "option1": "7g",
      "option2": null,
      "option3": null,
      "sku": "00628242240734",
      "requires_shipping": true,
      "taxable": true,
      "featured_image": null,
      "available": true,
      "name": "CBD Shark Shock - 7g",
      "public_title": "7g",
      "options": [
        "7g"
      ],
      "price": 6150,
      "weight": 0,
      "compare_at_price": 6300,
      "inventory_management": "shopify",
      "barcode": "00628242240734"
    },
    {
      "id": 12175424227148,
      "title": "15g",
      "option1": "15g",
      "option2": null,
      "option3": null,
      "sku": "00628242240741",
      "requires_shipping": true,
      "taxable": true,
      "featured_image": null,
      "available": true,
      "name": "CBD Shark Shock - 15g",
      "public_title": "15g",
      "options": [
        "15g"
      ],
      "price": 13180,
      "weight": 0,
      "compare_at_price": 13050,
      "inventory_management": "shopify",
      "barcode": "00628242240741"
    }
  ],
  "images": [
    "\\/\\/cdn.shopify.com\\/s\\/files\\/1\\/2636\\/1928\\/products\\/upload-100229-00628242240710-00.jpg?v=1562077121",
    "\\/\\/cdn.shopify.com\\/s\\/files\\/1\\/2636\\/1928\\/products\\/upload-100229-00628242240727-m1c0-en-compressed.jpg?v=1562101876"
  ],
  "featured_image": "\\/\\/cdn.shopify.com\\/s\\/files\\/1\\/2636\\/1928\\/products\\/upload-100229-00628242240710-00.jpg?v=1562077121",
  "options": [
    "Weight"
  ],
  "content": "Redecan’s CBD Shark Shock is a pairing of White Widow and Skunk #1. This strain, from Ontario’s Niagara region, has very mild THC potency and compact buds covered in resin. Its aroma is dank, fruity and sweet, and it tastes like green apple, sour citrus, sweet apricot and eucalyptus.\n\n"
}
```

## Sample single product json

```
{
  "id": 12175424128844,
  "title": "1g",
  "option1": "1g",
  "option2": null,
  "option3": null,
  "sku": "00628242240710",
  "requires_shipping": true,
  "taxable": true,
  "featured_image": null,
  "available": true,
  "name": "CBD Shark Shock - 1g",
  "public_title": "1g",
  "options": [
    "1g"
  ],
  "price": 975,
  "weight": 0,
  "compare_at_price": 900,
  "inventory_management": "shopify",
  "barcode": "00628242240710"
}
```

## BCCS response json

```
{
  "id": 2108512536400,
  "title": "TROPICAL BREEZE",
  "handle": "tropical-breeze-synrg",
  "description": "The fluffy, light green buds of this hybrid strain (White Widow) are covered in crystal resin that appears almost white. Tropical fruit aromas combine with a clean, sour lime aftertaste.",
  "published_at": "2019-06-03T08:31:45-07:00",
  "created_at": "2018-10-12T19:29:20-07:00",
  "vendor": "Canntrust Inc.",
  "type": "Cannabis",
  "tags": [
    "airship_restriction::",
    "b2b_order_limit::1000991=no_limit|1001007=no_limit|1001015=no_limit",
    "brand::SYNR.G",
    "class::Hybrid",
    "consumption_method::Inhalation",
    "country::CANADA",
    "gram_equivalency::1000991=1|1001007=3.5|1001015=7",
    "gtin::20841464000755",
    "potency::High THC (above 15%)",
    "potency::Low CBD (below 4%)",
    "potency_cbd::Low CBD (below 4%)",
    "potency_thc::High THC (above 15%)",
    "region::Ontario",
    "species::Hybrid",
    "subcategory::Flower",
    "vendor_name::"
  ],
  "price": 899,
  "price_min": 899,
  "price_max": 5799,
  "available": true,
  "price_varies": true,
  "compare_at_price": null,
  "compare_at_price_min": 0,
  "compare_at_price_max": 0,
  "compare_at_price_varies": false,
  "variants": [
    {
      "id": 19473069213520,
      "title": "1 g",
      "option1": "1 g",
      "option2": null,
      "option3": null,
      "sku": "1000991",
      "requires_shipping": true,
      "taxable": true,
      "featured_image": null,
      "available": false,
      "name": "TROPICAL BREEZE - 1 g",
      "public_title": "1 g",
      "options": [
        "1 g"
      ],
      "price": 899,
      "weight": 46,
      "compare_at_price": null,
      "inventory_management": "shopify",
      "barcode": "null"
    }
  ]
}
```
